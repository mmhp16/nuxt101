export const state = () => ({
    counter: 220
});

export const mutations = {
    increment(state) {
        state.counter++
    }
};

export const actions = {
    increment({commit}) {
        commit('increment');
        // console.log('context', context)
        return 213
    }
};