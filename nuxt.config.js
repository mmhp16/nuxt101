export default {
    // Global page headers (https://go.nuxtjs.dev/config-head)
    head: {
        title: 'nuxt',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: ''}
        ],
        link: [
            // {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
            // {rel: 'stylesheet', href: '@/assets/scss/app/app.scss'}
            // {rel: 'stylesheet', href: '/scss/app/app.scss'}
        ]
    },

    // Global CSS (https://go.nuxtjs.dev/config-css)
    css: [
        // '@/assets/scss/app/app.scss'
        '@/assets/scss/global.scss'
    ],

    // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
    plugins: ['~/plugins/isMobile.js'],

    // Auto import components (https://go.nuxtjs.dev/config-components)
    components: true,

    // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
    buildModules: [],

    // Modules (https://go.nuxtjs.dev/config-modules)
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        // https://go.nuxtjs.dev/pwa
        '@nuxtjs/pwa',
        '@nuxtjs/device',
        '@nuxtjs/router',

    ],
    // styleResources: {
    //     scss: [
    //         '~/assets/scss/app/app.scss'
    //     ]
    // },

    // Axios module configuration (https://go.nuxtjs.dev/config-axios)
    build: {
        extractCSS: true,
        publicPath: '/assets/',
        loaders: {
            cssModules: {
                modules: {
                    localIdentName: "[sha1:hash:hex:8]"
                }
            }
        },
        indicator: true,
        splitChunks: {
            layouts: true
        }
    },
    server: {
        port: 8000,
        host: '0.0.0.0' // default: localhost
    },
    filenames: {
        app: ({isDev}) => isDev ? '[name].js' : '[contenthash].js',
        chunk: ({isDev}) => isDev ? '[name].js' : '[contenthash].js',
        css: ({isDev}) => isDev ? '[name].css' : '[contenthash].css',
        img: ({isDev}) => isDev ? '[path][name].[ext]' : 'img/[contenthash:7].[ext]',
        font: ({isDev}) => isDev ? '[path][name].[ext]' : 'fonts/[contenthash:7].[ext]',
        video: ({isDev}) => isDev ? '[path][name].[ext]' : 'videos/[contenthash:7].[ext]'
    },
    html: {
        minify: {
            collapseBooleanAttributes: true,
            decodeEntities: true,
            minifyCSS: true,
            minifyJS: true,
            processConditionalComments: true,
            removeEmptyAttributes: true,
            removeRedundantAttributes: true,
            trimCustomFragments: true,
            useShortDoctype: true
        }
    },


}
