import Router from 'vue-router'

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/test1',
            component: () => import("@/customComponents/desktop/test/test1.vue").then((m) => m.default || m)
        },
        {
            path: '/test2',
            component: () => import("@/customComponents/desktop/test/test2.vue").then((m) => m.default || m)
        },
    ]
});