import Router from 'vue-router'

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/app',
            component: () => import("@/pages/app/index.vue").then((m) => m.default || m)
        },
        {
            path: '/test1',
            component: () => import("@/customComponents/mobile/test/test1.vue").then((m) => m.default || m)
        },
        {
            path: '/test2',
            component: () => import("@/customComponents/mobile/test/test2.vue").then((m) => m.default || m)
        },
    ]
});
